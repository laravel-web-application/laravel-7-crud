# Laravel 7 CRUD
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-crud.git`
2. Go inside the folder: `cd laravel-7-crud`
3. Run `cp .env.example .env`then create your desired database name
4. Run `php artisan migrate`
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Second Page

![Second Page](img/kedua.png "Second Page")

Create new Contact

![Create new Contact](img/create.png "Create new Contact")

List all contacts

![List all contacts](img/list.png "List all contacts") 
