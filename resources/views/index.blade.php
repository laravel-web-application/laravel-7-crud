@extends('base')
@section('content')
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            <h1>Anak IT - Tutorial Laravel 7.11.0</h1>
            <p>Apa saja yang akan kamu pelajari ?</p>

            <h2>* Templating Laravel 7.11.0</h2>
            <h2>* CRUD dengan Laravel 7.11.0 (Soon)</h2>
            <h2>* Upload Image dengan laravel 7.11.0 (Soon)</h2>
            <h2>* Login & Register dengan Auth & Session built in Laravel 7.11.0 (Soon)</h2>
            <h2>* Kirim E-mail dengan Laravel 7.11.0 (Soon)</h2>
            <h2>* Basic Web Service dengan Laravel 7.11.0 (Soon)</h2>
            <h2>* Laravel OAuth 2.0 dengan Laravel Passport 7.11.0 (Soon)</h2>
            <h2>* Perkenalan dengan Lumen (Microframework Laravel untuk REST API) (Soon)</h2>
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection
